package naivebayes;
import java.util.*;
import javagently.*;

/**
 * Using a naive Bayes classifier to distinguish utterances of the word yes from the word no
 */

public class YesNoClassifier {

    public static void main(String[] args)
    {
	// Read in MFCC data 
	
	String mfccDataDirectory = "data/yesno/mfcc/"; 
	Data yesData = new Data (mfccDataDirectory+"yes");
	Data noData = new Data (mfccDataDirectory+"no");

	// Build a naive Bayes classifier

	Classifier classifier = new Classifier(yesData.getMeanMfcc(),noData.getMeanMfcc());

	// Compute the probability of being in class one for the first yes example
	// using the 1st time-averaged MFCC as the feature

	int featureNumber = 0; // Using this MFCC component (0 is 1st component)
	int exampleNumber = 0; // Classifying this example

	// double answer = classifier.classify(yesData.getMeanMfcc(exampleNumber),featureNumber);
		double answer1;
		double answer2;
		int wrong1 = 0;
		int wrong2 = 0;
		for(exampleNumber = 0; exampleNumber<82; exampleNumber++)
		{
			answer1 = classifier.classify(yesData.getMeanMfcc(exampleNumber), featureNumber);
			answer2 = classifier.classify(yesData.getMeanMfcc(exampleNumber));
			if( answer1 < 0.5)
				wrong1++;
			if( answer2 < 0.5)
				wrong2++;

		}
		for(exampleNumber = 0; exampleNumber<83; exampleNumber++)		
		{
			answer1 = classifier.classify(noData.getMeanMfcc(exampleNumber), featureNumber);
			answer2 = classifier.classify(noData.getMeanMfcc(exampleNumber));
			if( answer1 > 0.5)
				wrong1++;
			if(answer2 > 0.5)
				wrong2++;
		}
		System.out.println("error rate before: " + (double)wrong1/165.0);
		System.out.println("error rate after: " + (double)wrong2/165.0);



    }
}
