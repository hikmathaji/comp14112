package markov;
import java.util.*;
import java.math.*;
import java.lang.reflect.*;

/**
 * Class with main method for presenting the results of the lab. At the moment this just reads in the sequence data 
 * and the corresponding state labels.
 */

public class Answer {

    public static void main(String[] args)
    {
	String mfccDataDirectory = "data/yesno_uncut/mfcc/"; 
	String labelDirectory = "data/yesno_uncut/labels/";

	// Read in the MFCC data and state labels from each class

	DataWithLabels dataClass1 = new DataWithLabels (mfccDataDirectory+"yes",labelDirectory+"yes");
	DataWithLabels dataClass2 = new DataWithLabels (mfccDataDirectory+"no",labelDirectory+"no");

	// Task 1

	// number of states is 3: leading silence, yes or no, trailing silence
	int noOfState = 3;

	// creating models for yes and no data
	HiddenMarkovModel hmmForYes = new HiddenMarkovModel(noOfState, dataClass1.getMfcc(), dataClass1.getLabels());
	HiddenMarkovModel hmmForNo = new HiddenMarkovModel(noOfState, dataClass2.getMfcc(), dataClass2.getLabels());

	// printing answers for "yes"
	System.out.println("Transition probablities for Yes data");
	for(int i = 0; i < noOfState; i++)
	{
		for(int j = 0; j < noOfState; j++)
		{
			System.out.print(hmmForYes.getTransitionProbability(i, j) + " ");
		}
		System.out.println();
	}
	System.out.println();

	// printing answers for "no" 
	System.out.println("Transition probablities for No data");
	for(int i = 0; i < noOfState; i++)
	{
		for(int j = 0; j < noOfState; j++)
		{
			System.out.print(hmmForNo.getTransitionProbability(i, j) + " ");
		}
		System.out.println();
	}
	System.out.println();
	

	// Task 2

	
	double p = (double) dataClass1.getNumberExamples()/(double)(dataClass1.getNumberExamples() + dataClass2.getNumberExamples());
	
	int numberOfErrors = 0;
	Classifier classifier = new Classifier(hmmForYes, hmmForNo, p);
	for(int i = 0; i< dataClass1.getNumberExamples(); i++)
	{
		if(classifier.classify(dataClass1.getMfcc(i)) < 0.5)
			numberOfErrors++;
	}

	System.out.println("Percentage of incorrect results for \"yes\" data: " + (double)numberOfErrors*100/(double)dataClass1.getNumberExamples());
    numberOfErrors = 0;
	
	for(int i = 0; i< dataClass2.getNumberExamples(); i++)
	{
		if(classifier.classify(dataClass2.getMfcc(i)) > 0.5)
			numberOfErrors++;
	}
	System.out.println("Percentage of incorrect results for \"no\" data: " + (double)numberOfErrors*100/(double)dataClass1.getNumberExamples());
    


	// Task 3

	HiddenMarkovModel combinedModel = new HiddenMarkovModel(4);
	double temp = hmmForYes.getTransitionProbability(0,0)+hmmForNo.getTransitionProbability(0,0);
	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			combinedModel.setTransitionProbability(0.0, i, j);
		}
	}

	combinedModel.setTransitionProbability(temp/2.0, 0, 0);
	combinedModel.setTransitionProbability(hmmForYes.getTransitionProbability(0, 1), 0, 1);
	combinedModel.setTransitionProbability(hmmForNo.getTransitionProbability(0,1), 0, 2);
	combinedModel.setTransitionProbability(hmmForYes.getTransitionProbability(1,1), 1, 1);
	combinedModel.setTransitionProbability(hmmForNo.getTransitionProbability(1,1), 2, 2);
	combinedModel.setTransitionProbability(hmmForYes.getTransitionProbability(1, 2), 1, 3);
	combinedModel.setTransitionProbability(hmmForNo.getTransitionProbability(1, 2), 2, 3);
	temp = hmmForYes.getTransitionProbability(2,2)+hmmForNo.getTransitionProbability(2,2);
	combinedModel.setTransitionProbability(temp/2.0, 3, 3);

	for(int i = 0; i < 4; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			System.out.print(combinedModel.getTransitionProbability(i, j) + " ");
		}
		System.out.println();
	}
	System.out.println();


	for(int i = 0; i< 13; i++)
	{
		Normal normalTemp = hmmForYes.getEmissionDensity(i, 0);
		normalTemp = normalTemp.combine(hmmForNo.getEmissionDensity(i, 0));
		combinedModel.setEmissionDensity(normalTemp, i, 0);
		combinedModel.setEmissionDensity(hmmForYes.getEmissionDensity(i, 1), i, 1);
		combinedModel.setEmissionDensity(hmmForNo.getEmissionDensity(i, 1), i, 2);
		normalTemp = hmmForYes.getEmissionDensity(i, 2);
		normalTemp = normalTemp.combine(hmmForNo.getEmissionDensity(i, 2));
		combinedModel.setEmissionDensity(normalTemp, i, 3);
	}

	// Task 4

      for(int i = 0; i < dataClass1.getMfcc().size(); i++) {
        yesCon = combinedModel.viterbi(dataClass1.getMfcc(i));
        /*for(int j = 0; j<yesCon.length; j++)
        	System.out.println(yesCon[j] + " ");
        System.out.println();*/
      }
      
      for(int i = 0; i < dataClass2.getMfcc().size(); i++) {
        noCon = combinedModel.viterbi(dataClass2.getMfcc(i));
      }

    }
}
